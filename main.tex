\documentclass[a4paper, 12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{commath}
\usepackage{xcolor}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{amsmath}
\usepackage{standalone}
\usepackage{circuitikz}
\usepackage{booktabs}

\newcommand{\ieq}[1]{\input{eq/#1.tex}}
\newcommand{\itable}[1]{\input{tables/#1.tex}}
\newcommand{\ifigure}[1]{\input{figures/#1.tex}}

\setlength{\jot}{1mm}

\title{
    Relazione di Fisica \\
    \large Circuito RLC
}
\author{
    Stevanato Giacomo
    \and
    Romeo Angelo
    \and
    Favaro Marco
    \and
    Dal Corso Riccardo
    \and
    Barison Matteo
}
\date{17 Dicembre 2018}

\begin{document}
    \maketitle

    \section*{}
    {
        \itable{generale}
    }
    \section{Premessa}
    {
        \paragraph{}
        {
            A differenza dei circuiti LC, in cui idealmente la resistenza è nulla, nei circuiti RLC la resistenza non è trascurabile e causa uno smorzamento delle onde armoniche. Questo è dato dal fatto che la resistenza tende a trasformare energia in calore, sottraendola al sistema.
        }
        \paragraph{}
        {
            In quest'esperienza siamo andati ad analizzare il comportamento di un circuito di questo tipo, cercando i vari parametri che li descrivono e un modo per ottenerli dai dati osservati.
        }
    }
    \section{Equazione descrittiva}
    {
        \ifigure{circuito}
        \paragraph{}
        {
            Applichiamo la seconda legge di Kirchhoff ponendo la somma delle differenze di potenziale uguale a $0$.
            \ieq{somma_potenziale}
            Possiamo ora portare tutto in funzione della carica del condensatore:
            \ieq{passaggio_itqt}
            Otteniamo un’equazione differenziale di secondo grado, che nel nostro caso ha come soluzione la seguente funzione:
            \ieq{qt}
            E di conseguenza la funzione che esprime la carica del condensatore è:
            \ieq{vt}
            I cui parametri sono:
            \ieq{def_k}
            \ieq{def_omega}
        }
        \paragraph{}
        {
            L'uguaglianza \eqref{eq:def_k} ci è stata data dal professore, mentre noi abbiamo dimostrato l'uguaglianza \eqref{eq:def_omega} con l'equazione \eqref{eq:dim_eq_diff_omega}.
        }
        \paragraph{}
        {
            Notiamo inoltre che la funzione \eqref{eq:qt} riprende una funzione sinusoidale, aggiungendo un fattore esponenziale decrescente che fa da fattore di smorzamento.
        }
    }
    \section{Analisi dei dati}
    {
        \paragraph{}
        {
            Noi abbiamo realizzato un circuito analogo in laboratorio, collegando il voltmetro al nostro computer con Data Studio per raccogliere i dati e analizzarli poi con GeoGebra. \href{./Modello esperienza.ggb}{[Link al file]}
        }
        \paragraph{}
        {
            Anche avendo $R$, ci è risultato difficile individuare direttamente i valori di $C$ e $L$ dal grafico (abbiamo il valore base di $L$ ma esso è moltiplicato grazie all’effetto del ferro, e il valore finale ci è quindi ignoto). Abbiamo quindi deciso di cercare prima il valore di $k$ e $\omega$:
            \begin{itemize}
                \item Abbiamo trovato il periodo $T$ osservando il grafico
                \item Abbiamo trovato $\omega$ secondo la relazione $\omega = \frac{2\pi}{T}$
                \item Grazie all'equazione \eqref{eq:dim_k} abbiamo trovato $k$ secondo la relazione \ieq{def_k2}
            \end{itemize}
        }
        \paragraph{}
        {
            Da qui abbiamo rappresentato il grafico per verificare corrispondesse. Abbiamo anche trovato i valori di $L$ e $C$, in funzione di $R$, $V_{max}$, $V_{min}$ e $T$. Per semplicità abbiamo posto \ieq{def_ln}
            \ieq{def_L}
            \ieq{def_C}
        }
        \paragraph{}
        {
            L'uguaglianza \eqref{eq:def_L} è stata dimostrata nell'equazione \eqref{eq:dim_L} mentre l'uguaglianza \eqref{eq:def_C} nell'equazione \eqref{eq:dim_C}.
        }
        \paragraph{}
        {
            Notiamo che mentre $V_{max}$, $V_{min}$ e $T$ sono caratteristici di ogni singolo grafico, $R$ è un parametro che ci permette di generare infinite soluzioni. Nel nostro modello GeoGebra abbiamo aggiunto delle formule per calcolare $L$ e $C$ al variare di $R$ e si può notare come il grafico generato da questi ultimi rimanga sempre lo stesso nonostante il loro variare.
        }
        \paragraph{}
        {
            Nel nostro caso $R = 2.5\Omega$, il che ci ha portato a ricavare $C = 3.3mF$ e $L = 170mH$. Visto che il valore di $L$ della bobina posta in aria era di $9mH$ possiamo dire che il ferro l'ha moltiplicato per $19$ e non per $100$ come ci aspettavamo.
        }
        \itable{es_RLC}
    }

    \section{Difetti del modello}
    {
        \paragraph{}
        {
            Abbiamo notato dei difetti nel grafico del nostro modello poiché dopo un po' di tempo esso tende ad andare fuori fase dai dati osservati. \par
            La causa potrebbe essere una imprecisione nel rilevamento dei dati oppure una corrispondenza non del tutto precisa tra il modello e il fenomeno. In particolare quest’ultimo difetto potrebbe essere dato da una perdita di energia del sistema mediante mezzi non calcolati (forse onde elettromagnetiche) oppure una variazione nel valore della resistenza causata da un suo surriscaldamento anche se ci sentiamo di escludere quest’ultima ipotesi visto che dai nostri calcoli l’energia totale dissipata risulta nell’ordine di $10^{-3}J$.
        }
    }
    \section{Curiosità}
    {
        \paragraph{}
        {
            Abbiamo realizzato un modello in GeoGebra che rappresenta l’andamento generico di un circuito RLC dati i valori di $R$, $L$ e $C$. \href{./Modello generico.ggb}{[Link al file]}
        }
    }
    \section{Conclusione}
    {
        \paragraph{}
        {
            A seguito di questa esperienza di laboratorio siamo riusciti ad analizzare l’andamento di un sistema relativo ad un circuito RLC a partire da dei dati raccolti sperimentalmente. Abbiamo anche fatto esperienza col calcolo differenziale applicandolo all’analisi di questo sistema.
        }
    }

    \newpage
    \section{Calcoli e dimostrazioni}
    {
        \subsection{Verifica della soluzione all'equazione differenziale e individuazione del parametro \texorpdfstring{$\omega$}{omega}}
        {
            \paragraph{}
            {
                Iniziamo col calcolare $q'(t)$ e $q''(t)$ a partire dalla formula \eqref{eq:qt} per $q(t)$. Per semplicità cerchiamo di sostituire $q(t)$ e $q'(t)$ quando possibile.
                \ieq{dqdt}
                \ieq{ddqdt}
            }
            \paragraph{}
            {
                Ora sostituiamo nell'equazione \eqref{eq:passaggio_itqt} e cerchiamo di ottenere un valore per $\omega$ che soddisfi l'equazione.
                \ieq{dim_eq_diff_omega}
    	    }
            \paragraph{}
            {
               Interessante notare come nel caso $R = 0 \Omega$, si ottenga $k = 0$ e $\omega = \frac{1}{\sqrt{L C}}$, dato in accordo con l'osservazione dei circuiti LC.
            }
        }
        \subsection{Calcolo di \texorpdfstring{$k$}{k} a partire dal grafico}
        {
            \paragraph{}
            {
                Supponendo che il circuito venga chiuso nell'istante $t_i$ possiamo dedurre che:
                \ieq{dim_k}
            }
        }
        \subsection{Calcolo di \texorpdfstring{$L$}{L} a partire dal grafico}
        {
            \paragraph{}
            {
                Per semplificare i calcoli poniamo \ieq{def_ln}
                \ieq{dim_L}
            }
        }
        \subsection{Calcolo di \texorpdfstring{$C$}{C} a partire dal grafico}
        {
            \paragraph{}
            {
                Per semplificare i calcoli poniamo nuovamente \ieq{def_ln}
                \ieq{dim_C}
            }
        }
    }

\end{document}
